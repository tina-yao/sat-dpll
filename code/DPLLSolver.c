//@file DPLLSolver.c
#include "Global.h"

/**
* @param  CHead **C（链表头结点指针的指针） 
* @return status（函数执行状态）
* @brief  DPLL算法核心部分，包括子句集的优化、变元选择的策略、递归实现等 
*/
status DPLLSolver(CHead **C)
{
	PHead dnow = NULL; 
	int uni = 0;
	while((dnow=isUniClause(*C))!=NULL){                                         //如果子句集中存在单子句，进入循环
		uni = dnow->right->data;                                                 //决策变元选择为单子句的变元 
		uni > 0 ? (counter[abs(uni)].value = 1) : (counter[abs(uni)].value = -1);//记录变元正负 
		Simplification(uni, C);                                                  //对子句集进行化简 
		if(*C==NULL) return TRUE;
		else if(isEmptyClause(*C)) return FALSE;
	}
	uni = strategy2(C);                                                          //基于一定策略来选择决策变元 
	uni > 0 ? (counter[abs(uni)].value = 1) : (counter[abs(uni)].value = -1);
	PHead copy1 = Duplication(C);                                                //对数据结构进行复制，为递归做好准备 
	AddUniClause(uni, &copy1);                                                   //将决策变元作为单子句加入子句集中 
	if(DPLLSolver(&copy1)) return TRUE;                                          //实现递归 
	else{
		PHead copy2 = Duplication(C);
		uni > 0 ? (counter[abs(uni)].value = -1) : (counter[abs(uni)].value = 1);
		AddUniClause(opp(uni), &copy2);
		return DPLLSolver(&copy2);
	}
}


/**
* @param  CHead *C（链表头结点的指针）
* @return CHead *C（子句头结点的指针），或者NULL
* @brief  根据存储在子句结构体的子句的变元数来判断是否存在单子句 
*/
CHead* isUniClause(CHead *C)
{
	CHead *temp = C;
	while(temp!=NULL){
		if(temp->info==1){//该子句变元数为1 
			return temp;
		}
		temp = temp->down;
	}
	return NULL;//无单子句，返回NULL 
}


/**
* @param  CHead *C（链表头结点的指针）
* @return status（函数执行状态） 
* @brief  根据存储在子句结构体的子句的变元数来判断是否存在空子句 
*/
status isEmptyClause(CHead *C)
{
	CHead *temp = C;
	while(temp!=NULL){
		if(temp->info==0){
			return OK;//有空子句，返回OK 
		}
		temp = temp->down;
	}
	return FALSE;//无空子句，返回FALSE
}


/**
* @param  int uni（决策变元），CHead **C（链表头结点的指针的指针）
* @return status（函数执行状态） 
* @brief  根据决策变元和单子句规则来优化子句集 
*/
status Simplification(int uni, CHead **C)
{
	DelClause(uni, C);//删除含有uni的整个子句 
	DelNode(uni, C);  //删除值为-uni的结点 
	return OK;
}


/**
* @param  int x（决策变元），CHead **C（链表头结点的指针的指针）
* @return status（函数执行状态） 
* @brief  删除含有变元x的整个子句
* @other  C是指针的地址，*C是指针
*/
status DelClause(int x, CHead **C)
{
	PHead p = *C, q = NULL;
	PNode m = NULL;
	int flag = 1;
	while(p!=NULL){
		m = p->right;
		while(m!=NULL){
			if(m->data==x){
				flag = 0;
				break;
			}
			m = m->next;
		}
		if(flag==1){
			break;//说明第一个子句不用删，退出循环 
		}
		else if(flag==0){//需要删除，且再次进入循环验证第一个子句 
			q = *C;
			*C = (*C)->down;
			p = *C;
			FreeCNodes(&q);
			free(q);
			q = NULL; 
			flag = 1;
		}
	}
	
	//接下来开始处理一般情况
	if(*C==NULL){
		return OK;
	}
	p = *C;
	q = p->down;
	flag = 0;
	while(q!=NULL){
		m = q->right;
		while(m!=NULL){
			if(m->data==x){
				flag = 1;
				p->down = q->down; 
				FreeCNodes(&q);//释放一个子句 
				free(q);
				q = NULL;
				break;
			}
			m = m->next;
		}
		if(flag==0){
			p = p->down;
			q = q->down;
		}
		else if(flag==1){
			q = p->down;
			flag = 0;
		}
	}
	return OK;
}


/**
* @param  CHead **node（待释放子句的头结点的指针的指针）
* @return status（函数执行状态） 
* @brief  释放一个子句的所有结点，包括它的变元结点和头结点
*/
status FreeCNodes(CHead **node) 
{
	CNode *p = NULL, *q = NULL;
	p = (*node)->right;
	q = p->next;
	if(q==NULL){
		free(p);
	}
	while(q!=NULL){
		free(p); 
		p = q;
		q = p->next;
	}
	return OK;
}


/**
* @param  int x（决策变元），CHead **C（链表头结点指针的指针）
* @return status（函数执行状态） 
* @brief  删除-x结点，并将该结点前后的变元结点连接起来 
*/
status DelNode(int x, CHead **C)
{
	PHead temp1 = *C;
	PNode temp2 = NULL, p = NULL, q = NULL; 
	while(temp1!=NULL){
		temp2 = temp1->right;
		while(temp2->data==opp(x)){//特殊情况，需要删掉第一个结点
			temp1->right = temp2->next;
			free(temp2);
			temp2 = temp1->right;
			temp1->info--;
		}
		p = temp2;
		q = p->next;
		while(q!=NULL){
			if(q->data==opp(x)){
				p->next = q->next;
				free(q);
				q = p->next;
				temp1->info--;
			}
			else{
				p = q;
				q = p->next;
			}
		}
		temp1 = temp1->down;
	}
	return OK;	
}


/**
* @param  CHead **C（链表头结点指针的指针）
* @return CHead *C（通过复制得到的副本的头结点的指针） 
* @brief  对链表进行复制以得到副本，为DPLL中的递归做好准备 
*/
CHead* Duplication(CHead **C)
{
	PHead p = *C, q = NULL, dt = NULL, dnow = NULL;//dt用来追踪正本中的子句头结点 
	PNode rnow = NULL, rt = NULL;//rt用来追踪正本中的变元结点 
	while(p!=NULL){
		dnow = IniClause(&q);
		dnow->info = p->info;
		rt = p->right;
		while(rt!=NULL){
			rnow = IniNode(&(dnow->right));
			rnow->data = rt->data;
			rt = rt->next;
		}
		p = p->down;
	}
	return q;
}

/**
* @param  int x（决策变元），CHead **C（链表头结点指针的指针）
* @return status（函数执行状态） 
* @brief  将决策变元作为单子句加入到子句集中，对子句集进行优化；
* @other  同时为了节约遍历时间，将新添加的单子句加入到链表的起始位置 
*/
status AddUniClause(int x, CHead **C)
{
	PHead ne = NULL;
	ne = (PHead)malloc(sizeof(CHead));
	if(!ne) exit(ERROR);
	ne->down = *C;
	ne->right = NULL;
	ne->info = 0;
	*C = ne;
	ne->info = 1;
	PNode node = IniNode(&(ne->right));
	node->data = x;
	counter[abs(x)].count += 1;
	if(x>0){
		counter[abs(x)].positive += 1;
	}
	else{
		counter[abs(x)].negative += 1;
	}
	node = IniNode(&(node->next));
	return OK;
}


/**
* @param  CHead **C（链表头结点指针的指针）
* @return int（选择的决策变元） 
* @brief  优化前的变元决策策略，将第一个子句的第一个变元作为决策变元 
*/
int strategy1(CHead **C)
{
	return (*C)->right->data;
}


/**
* @param  CHead **C（链表头结点指针的指针）
* @return int（选择的决策变元） 
* @brief  优化后的变元决策策略，基于子句长度、变元正负、变元出现次数来选择决策变元 
*/
int strategy2(CHead **C)
{
	PHead dnow = *C;
	PNode rnow = NULL;
	int x, pos = 0;
	double max = 0.0;
	while(dnow!=NULL){
		rnow = dnow->right;
		while(rnow!=NULL){
			x = rnow->data;
			if(counter[x].count>max){
				max = counter[x].count;
				pos = x;
			}
			rnow = rnow->next;
		}
		dnow = dnow->down;
	}
	if(pos==0) pos = (*C)->right->data;//用优化方法没找到决策变元时，使用strategy1 
	if(counter[pos].positive>counter[pos].negative) return pos;//正值频率大于负值，返回正值作为决策变元 
	else return opp(pos);
}


/**
* @param  CNF文件名 
* @return 函数执行结果 
* @brief  从主函数进入SAT的入口函数  
*/
int EntrySAT(char FileName[])
{
	PHead C = NULL;
	int re, i;
	clock_t start_t, end_t;
	double total_t;
	
	FileReader(&C, FileName);                                    //读取.cnf文件 
	start_t = clock();                                           //开始计时 
	re = DPLLSolver(&C);                                         //进行DPLL 
	end_t = clock();                                             //结束计时 
	total_t = (double)(end_t - start_t) / CLOCKS_PER_SEC * 1000; //计时换算
	SaveSATFile(re, total_t, FileName);                          //将DPLL结果存入到.res文件中 
	return OK;
}


/**
* @param  CHead **C（链表头结点指针的指针）
* @return int（选择的决策变元） 
* @brief  将DPLL结果输出到拓展名为.res的文件中 
*/
status SaveSATFile(int re, double t, char filename[])
{
	int i;
	for(i=0; filename[i]!='\n'; i++){//修改拓展名 
		if(filename[i]=='.'){
			filename[i+1] = 'r';
			filename[i+2] = 'e';
			filename[i+3] = 's';
		}
	}
	FILE *fp;
	fp = fopen(filename, "w");
	if(!fp) exit(-1);
	if(re==TRUE){
		fprintf(fp, "s 1\n");
		fprintf(fp, "v ");
		for(i=1; counter[i].data!=0 && i<MAXN; i++){
			if(counter[i].value==1){
				fprintf(fp, "%d ", i);
			}
			else if(counter[i].value==-1){
				fprintf(fp, "%d ", -i);
			}
		}
		fprintf(fp, "\n");
	}
	else{
		fprintf(fp, "s 0\n");
	}
	fprintf(fp, "t %f", t);
	fclose(fp);
	return OK;
}
