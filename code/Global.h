//@Global.h
#ifndef WORKSHOP_GLOBAL_H
#define WORKSHOP_GLOBAL_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

//Description of the macro
#define TRUE 1
#define FALSE 0
#define OK 1
#define ERROR -1 
#define MAXN 1200
#define COL 9//列 
#define ROW 9//行 
#define AC 0
#define WA -1

typedef int ElemType;
typedef int status;

//子句的变元结点的定义 
typedef struct CNode{  
      ElemType data;
      struct CNode *next;
}CNode, *PNode;

//子句的头结点的定义 
typedef struct CHead{
	ElemType info;//存储子句中的变元数量 
	struct CHead *down;
	struct CNode *right;
}CHead, *PHead;

typedef struct times{
	double count;
	ElemType data;
	double positive;
	double negative;
	ElemType value;//0：未赋值；1：赋正值：-1：赋负值 
}times;

times counter[MAXN];//计数器
int player[ROW][COL];

void test(CHead **C);
int opp(int x);

CHead* IniClause(CHead **C);
CNode* IniNode(CNode **right);
status FileReader(CHead **C, char filename[]);

status SaveSATFile(int re, double t, char filename[]);
status SaveSudokuFile(int re, char filename[]);

CHead* isUniClause(CHead *C);
status isEmptyClause(CHead *C);

status DPLLSolver(CHead **C);
CHead* Duplication(CHead **C);
int strategy2(CHead **C);
int strategy1(CHead **C);

status EntrySUDOKU(CHead **C);
int EntrySAT(char FileName[]);

void FirstRow(int firstrow[]);
status OtherRows(int rows[][COL], int i, int j);
void CreateSudoku(int rows[][COL]);
void CreatePlay(int a[][COL], int b[][COL], int numDigits);
status CreateSudokuToFile(char SudokuFile[], int holes);
status TranToCNF(int a[][COL], int holes, char SudokuFile[]);
void print(int a[][COL]);

#endif
