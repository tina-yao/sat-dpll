//@file FileReader.c
#include "Global.h"

/**
* @param  CHead **C（链表头结点指针的指针），filename（CNF文件名）
* @return status（函数执行状态）
* @brief  从CNF文件中读取数据，存入到数据结构中，将子句长度作为权重，结合变元出现次数来得到变元最终频率，
          子句长度越长，则权重越低；同时区分变元的正负，分别统计正负变元 
*/
status FileReader(CHead **C, char filename[])
{
	//以只读方式打开文件 
	FILE *fp = NULL;
	fp = fopen(filename, "r");
	if(!fp) exit(ERROR);
	
	//跳过注释行
	char ch;
	int num1, num2;
	while((ch=fgetc(fp))!='p'){//读到p跳出循环 
		if(ch=='c'){//跳过注释行 
			while((ch=fgetc(fp))!='\n'){
				;
			}
		}
	}
	while((ch=fgetc(fp))!='f'){
		;
	}
	//读取变元数和子句数
	fscanf(fp, "%d %d", &num1, &num2);
	
	//初始化变元计数器 
	int i;
	for(i=0; i<=num1; i++){
		counter[i].count = 0.0;
		counter[i].data = 0;
		counter[i].negative = 0.0;
		counter[i].positive = 0.0;
		counter[i].value = 0;//表明还未判断 
	}
	
	//读取文件，存入到数据结构中 
	int x;
	PHead dnow = NULL;
	PNode rnow = NULL;
	i = num2;
	while(i>0){
		i--;
		dnow = IniClause(C);
		do{
			fscanf(fp, "%d", &x);
			counter[abs(x)].data = abs(x);
			rnow = IniNode(&(dnow->right));
			rnow->data = x;
			dnow->info++;
		}while(x!=0);
	}
	fclose(fp);
	//test(C);
	//将子句长度作为权重来处理变元频率，子句长度越长，权重越低，权重函数：(1/2)^字句长度 
	i = num2;
	dnow = *C;
	while(i>0){
		i--;
		rnow = dnow->right;
		do{
			x = rnow->data;
			counter[abs(x)].count += pow(0.5, (double)(dnow->info - 1));
			if(x>0){
				counter[abs(x)].positive += pow(0.5, (double)(dnow->info - 1));
			}
			else{
				counter[abs(x)].negative += pow(0.5, (double)(dnow->info - 1));
			}
			rnow = rnow->next;
		}while(x!=0);
		dnow = dnow->down;
	}
	return OK;
}


/**
* @param  CHead **C（链表头结点指针的指针）
* @return CHead*（新生成子句的头结点指针）
* @brief  新生成子句的头结点，并将其正确连接到子句头结点链表的末尾上  
*/
CHead* IniClause(CHead **C)
{
	CHead *ne = NULL, *temp = *C;
	ne = (PHead)malloc(sizeof(CHead));
	if(!ne) return FALSE;
	ne->down = NULL;
	ne->right = NULL;
	ne->info = -1;//子句变元数初始化为-1，便于读取CNF文件时对一个字句含有变元数进行计数 
	if(*C==NULL){
		*C = ne;
		return ne;
	}
	while(temp->down!=NULL){
		temp = temp->down;
	}
	temp->down = ne;
	return ne;
}


/**
* @param  CNode **right
* @return CNode*（新生成变元结点的地址值）
* @brief  新生成变元结点，并将其正确连接到子句链表的末尾上 
*/
CNode* IniNode(CNode **right)
{
	CNode *ne = NULL, *temp = *right;
	ne = (PNode)malloc(sizeof(CNode));
	if(!ne) return FALSE;
	ne->next = NULL;
	ne->data = 0;
	if(*right==NULL){
		*right = ne;
		return ne;
	}
	while(temp->next!=NULL){//当temp->next==NULL时，退出循环 
		temp = temp->next;
	}
	temp->next = ne;
	return ne;
}


/**
* @param   x
* @return -x
* @brief  求相反数 
*/
int opp(int x)
{
	return -x;
}


/**
* @param  CHead **C（链表头结点指针的指针） 
* @return void
* @brief  用于验证CNF内部表示的数据结构和debug的测试函数
*/
void test(CHead **C)
{
	PHead temp1 = *C;
	PNode temp2 = NULL;
	while(temp1!=NULL){
		//printf("%d ", temp1->info);
		temp2 = temp1->right;
		while(temp2!=NULL){
			printf("%d ", temp2->data);
			temp2 = temp2->next;
		}
		printf("\n");
		temp1 = temp1->down;
	}
	printf("\n");
	/* 
	int i;
	for(i=111; i<729; i++){
		printf("data=%d\n", counter[i].data);
		printf("count=%f\n", counter[i].count);
		printf("positive=%f\n", counter[i].positive);
		printf("negative=%f\n", counter[i].negative);
		printf("counter[%d].value=%d\n",i, counter[i].value);
		printf("\n");
	}
	printf("\n");
	*/
	return ;
}
